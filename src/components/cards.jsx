import React, { Component } from "react";
import Card from "./card";
import DataSet from "./../data.json";
import Intro from "./intro";
import Guide from "./guide";
import Reference from "./reference";

class Cards extends Component {
  state = {
    cards: DataSet
  };

  render() {
    return (
      <React.Fragment>
        {this.state.cards.map(card => (
          <Card key={card.key} detail={card} />
        ))}
      </React.Fragment>
    );
  }
}

export default Cards;
