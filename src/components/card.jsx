import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Intro from "./intro";
import Reference from "./reference";
import Guide from "./guide";
class Card extends Component {
  state = {
    icon: this.props.detail.icon,
    title: this.props.detail.title,
    content: this.props.detail.content,
    intro_link: this.props.detail.intro_link,
    developer_guide_link: this.props.detail.developer_guide_link,
    api_reference_link: this.props.detail.api_reference_link
  };

  render() {
    return (
      <div className="col-md-4">
        <div className="card">
          <div className="card-body">
            <h5 className="card-title text-left">
              <i className={this.state.icon}></i>
              {this.state.title}
            </h5>
            <p className="card-text text-left">{this.state.content}</p>
          </div>

          <ul className="list-group">
            <li className="list-group-item d-flex justify-content-between align-items-center">
              Intro
              <span className=" badge-pill">></span>
            </li>
            <li className="list-group-item d-flex justify-content-between align-items-center">
              Developer Guide
              <span className="badge-pill">></span>
            </li>
            <li className="list-group-item d-flex justify-content-between align-items-center">
              API's references
              <span className="badge-pill">></span>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default Card;
