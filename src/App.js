import React from "react";
import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Cards from "./components/cards";

function App() {
  return (
    <div className="App">
      <nav className="navbar navbar-dark bg-primary custumizedHeader">
        <h2>Documentation</h2>
      </nav>
      <div className="container">
        <div className="row">
          <h2>APIs and Services</h2>
        </div>
        <div className="row">
          <h4>Access information on how to use forge APIs and Services</h4>
        </div>
        <div className="row">
          <Cards />
        </div>
      </div>
    </div>
  );
}

export default App;
